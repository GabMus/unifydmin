from gi.repository import Gtk
from .graphview import UnifydminGraphBox
from .tableview import UnifydminTableView
from .confManager import ConfManager
import subprocess

class SystemViewHeaderbar(Gtk.Bin):
    def __init__(self, system, **kwargs):
        super().__init__(**kwargs)
        self.system = system
        self.confman = ConfManager()
        
        self.builder = Gtk.Builder.new_from_resource(
            '/org/gabmus/unifydmin/ui/systemViewHeaderbar.glade'
        )

        self.headerbar = self.builder.get_object('systemViewHeaderbar')
        self.headerbar.set_title(self.system.name)
        self.headerbar.set_show_close_button(True)
        self.popover = self.builder.get_object('systemViewPopover')
        self.builder.get_object('systemUriLabel').set_text(
            f'{self.system.username}@{self.system.address}'
        )
        self.overflow_btn = self.builder.get_object('systemOverflowBtn')
        self.ssh_btn = self.builder.get_object('systemSshBtn')

        self.back_button = self.builder.get_object('systemBackBtn')

        self.add(self.headerbar)
        self.builder.connect_signals(self)

    def on_systemOverflowBtn_clicked(self, btn):
        self.popover.popup()

    def on_systemSshBtn_clicked(self, btn):
        cmd = f'{self.confman.conf["terminal_cmd"]} "ssh {self.system.username}@{self.system.address}"'
        if self.confman.is_flatpak:
            cmd = f'flatpak-spawn --host {cmd}'
        subprocess.Popen(cmd, shell=True)

    def on_systemViewDeleteBtn_clicked(self, btn):
        self.confman.del_system_by_name(self.system.name)

class UnifydminSystemView(Gtk.Bin):
    def __init__(self, system, **kwargs):
        super().__init__(**kwargs)
        self.system = system

        self.builder = Gtk.Builder.new_from_resource(
            '/org/gabmus/unifydmin/ui/systemView.glade'
        )

        self.headerbar = SystemViewHeaderbar(self.system)
        self.back_button = self.headerbar.back_button

        self.flowbox = self.builder.get_object('watchersFlowbox')
        self.tablebox = self.builder.get_object('tableBox')
        for w in self.system.watchers:
            if len(w.data) > 1:
                n_child = Gtk.FlowBoxChild()
                n_child.add(UnifydminGraphBox(w))
                n_child.set_valign(Gtk.Align.START)
                self.flowbox.add(n_child)
            else:
                n_child = UnifydminTableView(w)
                self.tablebox.pack_start(n_child, False, False, 6)
            #n_child.set_halign(Gtk.Align.FILL)
                
        self.back_button = self.headerbar.back_button
        self.add(self.builder.get_object('systemViewContainer'))

        self.builder.connect_signals(self)

    def on_shift_enter(self, *args):
        self.headerbar.on_systemSshBtn_clicked(None)
