from gi.repository import Gtk, Pango
from .initialsIcon import UnifydminInitialsIcon

class UnifydminSidebarListboxRow(Gtk.ListBoxRow):
    def __init__(self, name, **kwargs):
        super().__init__(**kwargs)
        self.name = name
        self.initials_icon = UnifydminInitialsIcon(self.name)
        self.container_box = Gtk.Box(Gtk.Orientation.HORIZONTAL)
        self.container_box.pack_start(self.initials_icon, False, False, 0)
        self.name_label = Gtk.Label(self.name)
        self.name_label.set_hexpand(False)
        self.name_label.set_ellipsize(Pango.EllipsizeMode.END)
        self.container_box.pack_start(self.name_label, False, False, 0)
        self.add(self.container_box)


class UnifydminSidebarSystemListBoxRow(UnifydminSidebarListboxRow):
    def __init__(self, system, **kwargs):
        super().__init__(name = system.name, **kwargs)
        self.system = system
        self.disconnected_icon = Gtk.Image.new_from_icon_name(
            'network-wired-disconnected-symbolic',
            Gtk.IconSize.MENU
        )
        self.disconnected_icon.set_tooltip_text(
            'System is unreachable'
        )
        self.container_box.pack_end(self.disconnected_icon, False, False, 6)
        self.disconnected_icon.set_no_show_all(True)

        self.system.connect(
            'unifydmin_system_disconnected',
            self.on_system_disconnected
        )
        self.system.connect(
            'unifydmin_system_reconnected',
            self.on_system_reconnected
        )

    def on_system_disconnected(self, *args):
        self.disconnected_icon.show()

    def on_system_reconnected(self, *args):
        self.disconnected_icon.hide()



class UnifydminSidebarActionListBoxRow(UnifydminSidebarListboxRow):
    def __init__(self, action, **kwargs):
        super().__init__(name = action.name, **kwargs)
        self.action = action
