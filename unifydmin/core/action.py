import threading
from collections import deque # deque is a list optimized for rotation

class Action:

    def __init__(self, name: str, description: str, arguments: list = None):
        self.name = name
        self.description = description
        self.arguments = arguments
        # stores the last result (needs to be fabric.runners.Result object)
        self.last_result = None

    def _async_worker(self, system):
        '''This function will be what the worker thread actually runs.'''
        raise NotImplementedError('This Action hasn\'t implemented _async_worker')

    def run(self, system) -> threading.Thread:
        t = threading.Thread(
            name = f'{type(self)} worker thread',
            group = None,
            target = self._async_worker,
            args = (system,) # NOTE: needs the trailing comma to be recognised as a tuple
        )
        t.start()
        return t

    def __repr__(self):
        return f'{self.name} - {self.description}'

class WatcherAction(Action):

    def __init__(self, name: str, description: str, data_size: int, system, arguments: list = None):
        self.__data = deque([0]*data_size)
        self.data_size = data_size
        self.system = system
        super().__init__(name=name, description=description, arguments=arguments)

    @property
    def data(self):
        return self.__data

    def _add_new_data(self, n_data):
        if self.data_size == 1:
            self.__data[0] = n_data
        else:
            self.__data.rotate(-1)
            self.__data[-1] = n_data

    def _worker(self):
        '''This function will be what the global watcher thread actually runs.'''
        raise NotImplementedError('This WatcherAction has not implemented _async_worker')

    def run(self, *args, **kwargs):
        raise NotImplementedError(
            'WatcherAction run method isn\'t and shouldn\'t be implemented'
        )

    def _async_worker(self, *args, **kwargs):
        raise NotImplementedError(
            'WatcherAction _async_worker method isn\'t and shouldn\'t be implemented'
        )

    def __eq__(self, other):
        return type(self) == type(other) and self.__dict__ == other.__dict__
