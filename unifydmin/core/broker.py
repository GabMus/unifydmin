from .action import WatcherAction

class Broker:
    '''Runs an action on a system and keeps its thread around to see if it's done'''

    def __init__(self, system, action):
        if type(action) == WatcherAction:
            raise TypeError('Broker requires an Action, but not a WatcherAction')
            return
        self.system = system
        self.action = action
        self.thread = None

    def run(self):
        self.thread = self.action.run(self.system)

    @property
    def done(self):
        return not self.thread.is_alive()

    def __repr__(self):
        return f'Broker - action {type(self.action)}; system {type(self.system)}'
