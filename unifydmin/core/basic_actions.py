from . import action
from time import sleep

def get_distro(system):
    release = system.run('cat /etc/lsb-release').stdout.split('\n')
    distro = {
        'os': '',
        'version': '',
        'description': ''
    }
    for row in release:
        if 'DISTRIB_ID' in row:
            distro['os'] = row.split('=')[1].strip('"')
        elif 'DISTRIB_RELEASE' in row:
            distro['version'] = row.split('=')[1].strip('"')
        elif 'DISTRIB_DESCRIPTION' in row:
            distro['description'] = row.split('=')[1].strip('"')
    return distro


def parse_boot_time(systemd_analyze_out):
    '''Takes systemd-analyze output, returns total boot time in seconds'''
    # TODO: relocate this function
    t = systemd_analyze_out.split('\n')[0].split('=')[-1].strip()
    tl = t.split(' ') # time list
    tn = float(tl[-1][:-1]) # time number
    if len(tl) == 2:
        tn += float(tl[0][:-3])*60.0
    elif len(tl) > 2:
        raise AttributeError('Unexpected time row')
        return
    return tn

class _dummy_result:
    def __init__(self, stdout, stderr):
        self.stdout = stdout
        self.stderr = stderr

class RebootSystem(action.Action):
    def __init__(self):
        super().__init__(
            name = 'Reboot System',
            description = 'Reboot the system',
            arguments = ''
        )

    def _async_worker(self, system):
        # get previous boot time to know how long reboot could take
        boot_time = parse_boot_time(system.run('systemd-analyze').stdout)
        system.run_as_root('reboot')
        self.last_result = _dummy_result('rebooted', '')
        sleep(boot_time + 20)
        online = False
        tries = 0
        while not online and tries < 5:
            try:
                tries += 1
                # dummy command to check if system is online
                online = system.run('true').return_code == 0
            except:
                sleep(20)
        assert online == True, f'System {system.name}: failed to reconnect after reboot'


class UpdateSystem(action.Action):
    def __init__(self):
        super().__init__(
            name = 'Update System',
            description = 'Updates the packages, has built-in middleware',
            arguments = None
        )

    def _async_worker(self, system):
        distro = get_distro(system)
        distroname = distro['os'].lower()
        # TODO: need to check validity of distro names
        # TODO: need to implement **BETTER** privilage elevation
        #       current method uses a new root connection (see system.py System.run_as_root)
        #       reason: root login could be disabled and in general is not good practice
        # TODO: missing: opensuse, gentoo
        if distroname in ['debian', 'ubuntu']:
            update_cmd = 'apt update && apt upgrade -y'
        elif distroname in ['arch', 'manjaro', 'antergos', 'kaos']:
            update_cmd = 'pacman -Syu --noconfirm'
        elif distroname in ['fedora', 'rhel', 'centos']:
            # check if the system has dnf, else fallback to yum
            if system.run('command -v dnf').return_code == 0:
                update_cmd = 'dnf upgrade --refresh -y'
            else:
                update_cmd = 'yum update -y'
        else:
            raise NotImplementedError(f'{distroname} currently unsupported')
            return
        self.last_result = system.run_as_root(update_cmd)

class DockerProcesses(action.WatcherAction):
    def __init__(self, system):
        self.has_docker = -1 # -1: unknown; 0: false; 1: true
        super().__init__(
            name = 'Docker Processes',
            description = 'Watches Docker containers status',
            data_size = 1,
            system = system,
            arguments = None
        )
        self.columns = [
            'name',
            'image',
            'command',
            'created',
            'status',
            'ports'
        ]

    def _worker(self):
        if self.has_docker == -1:
            self.has_docker = 1 if self.system.run('command -v docker').return_code == 0 else 0
            if self.has_docker == 0:
                self._add_new_data(None)
        elif self.has_docker == 0:
            return
        else: # self.has_docker == 1
            result = self.system.run(
                'docker ps -a --no-trunc --format "table {{.Names}};{{.Image}};{{.Command}};{{.RunningFor}};{{.Status}};{{.Ports}}"'
            )
            # example output:
            # IMAGE;COMMAND;CREATED;STATUS;PORTS
            # prestashop/prestashop;"docker-php-entrypoint /tmp/docker_run.sh";4 months ago;Up 2 weeks;0.0.0.0:8080->80/tcp
            # mysql:5.7;"docker-entrypoint.sh mysqld";4 months ago;Up 2 weeks;33060/tcp, 0.0.0.0:3307->3306/tcp

            # Checks for errors in the command that has just been ran
            # If there is any error an AssertionError is raised
            assert result.return_code == 0, f'Docker command returned {result.return_code}\n\nstdout:\n{result.stdout}\n\nstderr:\n{result.stderr}'
            containers = []
            rows = result.stdout.strip().split('\n')[1:]
            for row in rows:
                values=row.split(';')
                containers.append({
                    'name': values[0],
                    'image': values[1],
                    'command': values[2],
                    'created': values[3],
                    'status': values[4],
                    'ports': values[5]
                })
            self._add_new_data(containers)


class SystemdServices(action.WatcherAction):
    
    def __init__(self, system):
        super().__init__(
            name = 'Systemd Services',
            description = 'Watches all systemd services and their status',
            data_size = 1,
            system = system,
            arguments = None
        )
        self.columns = [
            'unit',
            'load',
            'active',
            'sub',
            'description'
        ]

    def _worker(self):
        result = self.system.run(
            'systemctl list-units --full --all --no-pager --plain --no-legend | grep -E "(\.service|\.socket)"'
        )
        rows = result.stdout.split('\n')[:-7]
        data = []
        for row in rows:
            row = row.strip().split(None, 4)
            data.append({
                'unit': row[0],
                'load': row[1],
                'active': row[2],
                'sub': row[3],
                'description': row[4]
            })
        self._add_new_data(data)
        # TODO: need better output from systemctl


class OpenPorts(action.WatcherAction):
    def __init__(self, system):
        super().__init__(
            name = 'Open Ports',
            description = 'Watches open ports (runs `ss -tulpn`)',
            data_size = 1,
            system = system,
            arguments = None
        )
        self.columns = [
            'netid',
            'state',
            'recvq',
            'sendq',
            'localaddressport',
            'peeraddressport',
            'process'
        ]

    def _worker(self):
        result = self.system.run_as_root(
            'ss -tulpn'
        )
        rows = result.stdout.split('\n')[1:]
        data = []
        for row in rows:
            row = row.strip().split(None, 6)
            if len(row) >= 7:
                data.append({
                    'netid': row[0],
                    'state': row[1],
                    'recvq': row[2],
                    'sendq': row[3],
                    'localaddressport': row[4],
                    'peeraddressport': row[5],
                    'process': row[6]
                })
        self._add_new_data(data)


class CpuUsage(action.WatcherAction):
    
    def __init__(self, system):
        self.idl_jiffies_0 = 0
        self.idl_jiffies_1 = 0
        self.tot_jiffies_0 = 0
        self.tot_jiffies_1 = 0
        super().__init__(
            name = 'CPU Usage',
            description = 'Watches CPU usage',
            data_size = 120,
            system = system,
            arguments = None
        )

    def _worker(self):
        self.tot_jiffies_0 = self.tot_jiffies_1
        self.idl_jiffies_0 = self.idl_jiffies_1
        self.tot_jiffies_1 = 0
        self.idl_jiffies_1 = 0

        result = self.system.run('head -n 1 /proc/stat').stdout
        stat = result.strip().split()[1:-2]
        self.idl_jiffies_1 = int(stat[3]) + int(stat[4])
        for j in stat:
            self.tot_jiffies_1 += int(j)
        self._add_new_data(
            (1.0 - ((self.idl_jiffies_1 - self.idl_jiffies_0) / (self.tot_jiffies_1 - self.tot_jiffies_0))) * 100.0
        )

class MemoryUsage(action.WatcherAction):

    def __init__(self, system):
        super().__init__(
            name = 'Memory Usage',
            description = 'Watches RAM usage',
            data_size = 120,
            system = system,
            arguments = None
        )

    def _worker(self):
        result = self.system.run('cat /proc/meminfo').stdout
        mem = result.split('\n')
        # values are in kibibytes (powers of 1024)
        total = 0
        free = 0
        used = 0
        swap_total = 0
        swap_free = 0
        swap_used = 0
        for row in mem:
            row=row.split()
            if len(row) >= 2:
                if 'memtotal' in row[0].lower():
                    total = int(row[1])
                elif 'memfree' in row[0].lower():
                    free = int(row[1])
                elif 'swaptotal' in row[0].lower():
                    swap_total = int(row[1])
                elif 'swapfree' in row[0].lower():
                    swap_free = int(row[1])

        used = total - free
        swap_used = swap_total - swap_free

        # multiplying by 1024 to convert to bytes
        #self._add_new_data(
        #    {
        #        'total': total*1024,
        #        'used': used*1024,
        #        'swaptotal': swap_total*1024,
        #        'swapused': swap_used*1024
        #    }
        #)
        self._add_new_data((used*100.0)/total)
