from gi.repository import Gtk, Handy
from .sidebar import UnifydminSidebar
from .appStack import UnifydminAppStack

class UnifydminLeaflet(Handy.Leaflet):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # base leaflet behavior options
        self.set_mode_transition_type(Handy.LeafletModeTransitionType.SLIDE)
        self.set_child_transition_type(Handy.LeafletModeTransitionType.SLIDE)
        self.set_interpolate_size(True)
        self.set_homogeneous(Handy.Fold.FOLDED, Gtk.Orientation.HORIZONTAL, False)
        self.set_homogeneous(Handy.Fold.UNFOLDED, Gtk.Orientation.HORIZONTAL, False)

        self.sidebar = UnifydminSidebar()

        self.main_stack = UnifydminAppStack()
        
        self.add(self.sidebar)
        self.add(self.main_stack)

    @property
    def folded(self):
        return self.get_fold() == Handy.Fold.FOLDED

    def switch_visible(self, *args):
        if self.get_visible_child() == self.sidebar:
            self.set_visible_child(self.main_stack)
        else:
            self.set_visible_child(self.sidebar)

    def set_visible_by_name(self, name):
        if name == 'sidebar':
            self.set_visible_child(self.sidebar)
        elif name == 'stack':
            self.set_visible_child(self.main_stack)
        else:
            raise ValueError('You need to pass either \'sidebar\' or \'stack\'')

