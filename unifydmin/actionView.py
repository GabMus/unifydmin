from gi.repository import Gtk, GObject
from .confManager import ConfManager
from .selectSystemsListBoxRow import UnifydminSelectSystemsListBoxRow
from .core.broker import Broker

class ActionViewHeaderbar(Gtk.Bin):
    def __init__(self, action, **kwargs):
        super().__init__(**kwargs)
        self.action = action

        self.builder = Gtk.Builder.new_from_resource(
            '/org/gabmus/unifydmin/ui/actionViewHeaderbar.glade'
        )

        self.headerbar = self.builder.get_object('actionViewHeaderbar')
        self.headerbar.set_title(self.action.name)
        self.headerbar.set_show_close_button(True)
        self.popover = self.builder.get_object('actionOverflowPopover')
        self.builder.get_object('actionDescriptionLabel').set_text(self.action.description)
        # select_all signal is connected in UnifydminActionView below
        self.select_all_btn = self.builder.get_object('selectAllBtn')
        # run signal is connected in UnifydminActionView below
        self.run_btn = self.builder.get_object('actionRunBtn')

        self.back_button = self.builder.get_object('actionBackBtn')

        self.add(self.headerbar)

        self.builder.connect_signals(self)

    def on_actionOverflowBtn_clicked(self, btn):
        self.popover.popup()


class UnifydminActionView(Gtk.Bin):
    
    __gsignals__ = {
        'unifydmin_action_running_changed': (
            GObject.SIGNAL_RUN_FIRST,
            None,
            (bool,)
        )
    }

    def __init__(self, action, **kwargs):
        super().__init__(**kwargs)
        self.action = action

        self.confman = ConfManager()
        self.builder = Gtk.Builder.new_from_resource(
            '/org/gabmus/unifydmin/ui/actionView.glade'
        )
        self.add(self.builder.get_object('actionViewContainer'))
        self.headerbar = ActionViewHeaderbar(self.action)

        self.headerbar.select_all_btn.connect('clicked', self.on_selectAllBtn_clicked)
        self.headerbar.run_btn.connect('clicked', self.on_actionRunBtn_clicked)
        self.systems_listbox = self.builder.get_object('chooseSystemsListbox')
        self.back_button = self.headerbar.back_button

        self.builder.connect_signals(self)
        self.populate_systems_list()
        
    def populate_systems_list(self, *args):
        while True:
            row = self.systems_listbox.get_row_at_index(0)
            if row:
                self.systems_listbox.remove(row)
            else:
                break
        for s in self.confman.systems:
            row = UnifydminSelectSystemsListBoxRow(s, self.action)
            self.systems_listbox.add(row)
        self.systems_listbox.show_all()

    def on_selectAllBtn_clicked(self, btn):
        for row in self.systems_listbox.get_children():
            if not row.selected:
                row.toggle_selected()
        self.headerbar.popover.popdown()

    def on_actionRunBtn_clicked(self, btn):
        btn.set_sensitive(False)
        btn.set_label('Running')
        self.emit('unifydmin_action_running_changed', True)
        running = []
        for row in self.systems_listbox.get_children():
            if row.selected:
                row.run_action()
                running.append(row)
        keep_running = True
        while keep_running:
            keep_running = False
            while Gtk.events_pending():
                Gtk.main_iteration()
            for row in running:
                if row.broker.done and row.action.last_result:
                    row.set_output(row.action.last_result.stdout)
                    row.done_running()
                else:
                    keep_running = True
        btn.set_sensitive(True)
        self.systems_listbox.set_sensitive(True)
        btn.set_label('Run')
        self.emit('unifydmin_action_running_changed', False)

    def on_chooseSystemsListbox_row_activated(self, listbox, row):
        row.toggle_selected()

    def on_shift_enter(self, *args):
        self.on_actionRunBtn_clicked(self.headerbar.run_btn)
